import requests, sys, time, logging, json

logging.basicConfig(level=logging.ERROR)

def timestr(t):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(t))

def parsetime(s):
    if s.endswith(".000Z"):
        s=s[:-5]
    return time.mktime(time.strptime(s, "%Y-%m-%dT%H:%M:%S"))

DERIBIT_API_URL = "https://deribit.com/api/v2"
FUNDING_INTERVAL = 8*60*60 # daily rate
INDICATIVE_FUNDING_INTERVAL = 15*60 # 15 min
class DeribitClient(object):
    def get_funding(self, symbol, indicative_interval=INDICATIVE_FUNDING_INTERVAL, **kwargs):
        end_ts = round(time.time())
        prev_rate = self.get_funding_rate_value_(symbol, end_ts - FUNDING_INTERVAL, end_ts)
        indicative_rate = self.get_funding_rate_value_(symbol, end_ts - indicative_interval, end_ts) / indicative_interval * FUNDING_INTERVAL
        return dict(
            prev_rate = prev_rate, # aggregated during last 8h
            next_rate = 0, # nothing is paid in the end - continuos rate
            indicative_rate = indicative_rate, # use smaller indicative interval for estimate
            begin_ts=end_ts-FUNDING_INTERVAL, end_ts=end_ts)
    
    def get_perpetual_contract(self, currency):
        return currency+"-PERPETUAL"

    def get_funding_rate_value_(self, symbol, begin_ts, end_ts):
        params = dict(instrument_name=symbol,start_timestamp=begin_ts*1000,end_timestamp=end_ts*1000)
        method = "public/get_funding_rate_value"
        url = DERIBIT_API_URL+"/"+method
        logging.info("%s %s", method, params)
        res = requests.get(url, params = params).json()
        logging.info("response %s", res)
        if 'error' in res:
            raise UserWarning('DERIBIT:'+res['message'])
        return res["result"]

BITMX_API_URL="https://www.bitmex.com/api/v1"
class BitmexClient(object):
    def get_funding(self, symbol):
        res = self.instrument_(symbol)
        next_rate = res["fundingRate"]
        indicative_rate = res["indicativeFundingRate"]
        end_ts = parsetime(res["fundingTimestamp"])
        return dict(next_rate=next_rate,  # paid on end_ts if holding instrument position in (begin_ts, end_time)
            prev_rate=next_rate, # it was calculated on previous period, so the same here
            indicative_rate=indicative_rate,
            begin_ts=end_ts - 8*3600, end_ts=end_ts)
    
    def get_perpetual_contract(sels, currency):
        if currency=="BTC": currency="XBT"
        return currency+"USD"

    def instrument_(self, symbol):
        params = dict(symbol=symbol, count=1)
        method = "instrument"
        url = BITMX_API_URL+"/"+method
        logging.info("%s %s", method, params)
        res = requests.get(url, params = params).json()
        logging.info("response %s", res)
        return res[0]


def get_client(exchange):
    if exchange=="DERIBIT": return DeribitClient()
    if exchange=="BITMEX": return BitmexClient()
    raise UserWarning("exchange {} not supported ".format(exchange))

if __name__=="__main__":
    if len(sys.argv)<2:
        print(format("usage: python funding.py CURRENCY [CURRENCY]...\nCURRENCY: BTC, ETH"))
        exit(1)
    print("INDICATIVE_FUNDING_INTERVAL(minutes)=%s"%(INDICATIVE_FUNDING_INTERVAL/60))
    print("FUNDING_INTERVAL(hours)=%s"%(FUNDING_INTERVAL/60/60))
    print("")
    print("%12s, %12s, %12s%%, %12s%%, %12s%%, %12s, %12s, %12s" %("EXCHANGE","CURRENCY", "NEXT", "PREV", "INDIC", "START", "END", "CONTRACT")) 
    for i in range(1,len(sys.argv)):
        currency = sys.argv[i]
        for exchange in ["DERIBIT","BITMEX"]:    
            client = get_client(exchange)
            contract = client.get_perpetual_contract(currency)
            funding = client.get_funding(contract)
            print("%12s, %12s, %12.6s%%, %12.6s%%, %12.6s, %12s, %12s, %12s"%(
                exchange, currency,
                funding["next_rate"]*100, funding["prev_rate"]*100, funding["indicative_rate"]*100,
                timestr(funding["begin_ts"]), timestr(funding["end_ts"]), contract))




