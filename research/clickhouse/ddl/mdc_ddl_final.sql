/* integer version. gives ~ 17 bytes per row */
CREATE TABLE IF NOT EXISTS market_by_price (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
    type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50, 'IMPLIED_BID' = 69, 'IMPLIED_ASK' = 70) DEFAULT 'UNDEFINED' Codec(LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(LZ4), \
    price_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    price_i Int64 DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    qty_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty_i  Int64  DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    num_orders UInt16 Codec(Gorilla,LZ4), \
    trade_id String Codec(LZ4)
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno, rowno) \
PRIMARY KEY (receive_time_utc, seqno, rowno)
