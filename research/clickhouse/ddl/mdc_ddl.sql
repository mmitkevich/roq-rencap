-- clickhouse tables common for ROQ_RENCAP project

/* Design notes:
1) ASCII codes '0' 48 '1' 49 '2' 50 'E' 69 'F' 70 are used for Enums (FIX-compatbile)
2) separate tables for market_by_price and market_by_order (since both could present simultaneously)
3) trades are written in market_by_price/market_by_order with type='TRADE' - hence trade_id column
4) set optimize_read_in_order=1
5) queries always should have order by
6) primary key allows same key values for many rows
7) ORDER BY implies PRIMARY KEY. so we're hopefully fast for receive_time_utc BETWEEN queries
8) 

** Questinable:
1) no index by symbol so all symbols are always read. We could always split into several tables 
   market_by_price_NASDAQ_100, market_by_price_CME_METALS, etc
*/

/* integer version. gives ~ 17 bytes per row */
CREATE TABLE IF NOT EXISTS market_by_price (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
    type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50, 'IMPLIED_BID' = 69, 'IMPLIED_ASK' = 70) DEFAULT 'UNDEFINED' Codec(LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(LZ4), \
    price_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    price_i Int64 DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    qty_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty_i  Int64  DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    num_orders UInt16 Codec(Gorilla,LZ4), \
    trade_id String Codec(LZ4)
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno, rowno) \
PRIMARY KEY (receive_time_utc, seqno, rowno)

CREATE TABLE IF NOT EXISTS reference_data (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
    security_type LowCardinality(String),\
    description String,\
    currency LowCardinality(String),\
    settlement_currency LowCardinality(String),\
    commission_currency LowCardinality(String),\
    tick_size Float64 DEFAULT toFloat64(nan),\
    multiplier Float64 DEFAULT toFloat64(nan),\
    min_trade_vol Float64 DEFAULT toFloat64(nan),\
    option_type LowCardinality(String),\
    strike_currency LowCardinality(String), \
    strike_price Float64 DEFAULT toFloat64(nan), \
    underlying LowCardinality(String), \
    time_zone LowCardinality(String), \
    issue_time_utc DateTime64(9), \
    settlement_time_utc DateTime64(9), \
    expiry_time_utc DateTime64(9), \
    ext Nested (\
        key LowCardinality(String),
        value String
    ) \
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno) \
PRIMARY KEY (receive_time_utc, seqno)



/* float version gives ~23.49 bytes per row */
CREATE TABLE IF NOT EXISTS market_by_price_float (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
        type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50, 'IMPLIED_BID' = 69, 'IMPLIED_ASK' = 70) DEFAULT 'UNDEFINED' Codec(LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(LZ4), \
    price Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    num_orders UInt16 Codec(Gorilla,LZ4), \
    trade_id String Codec(LZ4)
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno, rowno) \
PRIMARY KEY (receive_time_utc, seqno, rowno)

/* market_by_order for L3 data */
CREATE TABLE IF NOT EXISTS market_by_order ( \
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String),\
    symbol LowCardinality(String),\
    type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50) DEFAULT 'UNDEFINED' Codec(DoubleDelta, LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(DoubleDelta, LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(DoubleDelta, LZ4), \
    price Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty  Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    num_orders UInt16 Codec(DoubleDelta, LZ4), \
    trade_id String, \
    order_id UInt64 Codec(DoubleDelta, LZ4), \
    order_priority UInt32 Codec(doubleDelta, LZ4) \
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
PRIMARY KEY (receive_time_utc)
ORDER BY (receive_time_utc, seqno, rowno);

/* Separate trades could give benefit of separate index for them.
 However, it will require UNION ALL for selection by, say, backtester. And it complicates things.
 additional columns are almost free while they are empty. So I drop this idea

CREATE TABLE IF NOT EXISTS trades (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64  Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean ALIAS 0, \
    exchange LowCardinality(String),\
    symbol LowCardinality(String),\
    type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50) ALIAS 'TRADE',\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) ALIAS 'NEW',\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(DoubleDelta, LZ4), \    
    price Float64 DEFAULT toFloat64(nan),\
    qty  Float64 DEFAULT toFloat64(nan),\
    num_orders UInt16 Codec(DoubleDelta, LZ4),\
    trade_id String
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
PRIMARY KEY (receive_time_utc)
ORDER BY (receive_time_utc, seqno, rowno);

CREATE VIEW IF NOT EXISTS market_by_price_with_trades AS select* from ((SELECT \
    gateway, session_id, seqno, rowno, receive_time_utc, exchange_time_utc, snapshot, exchange, symbol, type, action, side, price, qty, num_orders, trade_id \
FROM market_by_price) UNION ALL (SELECT \
    gateway, session_id, seqno, rowno, receive_time_utc, exchange_time_utc, snapshot, exchange, symbol, type, action, side, price, qty, num_orders, trade_id \
FROM trades));
*/

/*
Various notes:
1. https://clickhouse.tech/docs/en/engines/table-engines/mergetree-family/mergetree/#choosing-a-primary-key-that-differs-from-the-sorting-key
2. https://altinitydb.medium.com/new-encodings-to-improve-clickhouse-efficiency-927678e496bd

2. comments on fields
CREATE TABLE IF NOT EXISTS MarketByPriceRaw (\
    gateway LowCardinality(String),  \             -- "roq-bitmex", "b2b-cme-mdp3", etc
    session_id UUID, \                              -- unique id of session (e.g. generated when connecting to the exchnage)
    seqno UInt64,  \                               -- assigned EITHER by GATEWAY or by EXCHANGE, increases monotonically (**1)
    rowno UInt32, \                                -- row index (per seqno). Assigned in such way that (seqno, recno) is unique (**2)
--        source LowCardinality(String),                -- EXCHANGE/BROKER
    receive_time_utc DateTime64(9), \              -- by STRATEGY.
--        receive_time UInt64,                          -- by STRATEGY (monotonic clock)
--        source_send_time UInt64,                      -- by GATEWAY/DATA COLLECTOR to CLIENT (monotonic clock)
--        source_receive_time UInt64,                   -- by GATEWAY/DATA COLLECTOR from EXHCANGE (monotonic clock)
--        origin_create_time UInt64,                    -- by EXCHANGE/BROKER (monotonic)
--        origin_create_time_utc DateTime64(9),         -- by EXCHNAGE/BROKER ? (realtime)
    exchange_time_utc DateTime64(9),\               -- original EXCHANGE timestamp
--        is_last Boolean,                              -- is last message in the batch
--        stream_id UInt16,                             -- Q: what is this?
    snapshot Boolean,  \                           -- is it snapshot or update
    exchange LowCardinality(String),  \            -- EXHCANGE name ("BITMEX", "CME")
    symbol LowCardinality(String),   \             -- insturment name ("XBTUSD", "ESH1")
    type LowCardinality(String),    \              -- MdEntryType "0=Bid" "1=Offer" "2=Trade Summary" FIXME: change to ENUM
    action LowCardinality(String),  \              -- MDUpdateAction "0=Add" "1=Change" "2=Delete" FIXME: change to ENUM
    price Float64, \                               -- MDEntryPx
    qty  Float64,  \                               -- MDEntrySize
    display_qty Float64,  \                        -- MDDisplayQty
    num_orders UInt16  \                           -- NumberOfOrders
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc)         -- this will be stored one week of data per parition 
-- ORDER BY (session_id, intHash64(seqno));       -- index contains session id and seqno
PRIMARY KEY (receive_time_utc);                   --  (**3)
ORDER BY (receive_time_utc, seqno, rowno);


-- **1) seqno is assigned either by EXCHANGE (e.g. UDP packet seqno), or by GATEWAY (unique counter as per sessionid).
--      seqno is monotonically increasing inside the session
-- **2) We need separate rowno since we could have several records per one seqno. So rowno is an index in the repeating group
-- **3) Original(@roq) ordering is (session_id, intHash64(seqno)) without primary key  (so insertion order is preserved)
--      Proposal(@mmitkevich) is to replace with PRIMARY_KEY (receive_time_utc) so that data is always sorted on receive time.
--      Reasons:
--        1. make SELECT WHERE receive_time_utc BETWEEN DATES faster
--        2. if two records have same receive_time_utc, we can decide which is first by comparing (seqno, rowno) tuple
*/

