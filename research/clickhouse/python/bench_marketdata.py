from clickhouse_driver import Client
from datetime import datetime, timedelta
import numpy as np
from types import SimpleNamespace
import argparse
import pandas as pd


ap = argparse.ArgumentParser()
ap.add_argument('--number', metavar='N', type=int, default=100000, help='number of rows in batch')
ap.add_argument('--repeat', metavar='R', type=int, default=3, help='number of repeats')
args = ap.parse_args()

client = Client(host="localhost", settings={'use_numpy': True})   

TABLE="market_by_price_float"
#TABLE="trades"

if True:
    client.execute("TRUNCATE TABLE %s"%TABLE)

now = datetime.now()
N = args.number
R = args.repeat
SIGMA = 0.001
#PRICE = 100.
VOL = 50.
TYPES = [0,0,0,0,0,1,1,1,1,1,2]
ACTIONS = []
symbols = ["MSFT","AAPL","BTC","LTC","XRP","INTC","USD","EUR","XOM","TSLA"]*20+["BTC","LTC","XRP","INTC"]*10 + ["XRP"]*5
exchanges = ["CME","BITMEX","BINANCE","DERIBIT"]
PRICE = [80]*10+[100.]*10+[20.]*10+[1000.]*5;

for r in range(R):
    print("generating %s records..."%N)
    cum_rand = np.cumsum(np.random.rand(N))
    exch_ts = (pd.timedelta_range(freq='10s', start=0,periods=N)+pd.to_datetime("2000-01-01").tz_localize('UTC')).values + np.random.randint(10000000000,size=N)
    recv_ts = (pd.timedelta_range(freq='10s', start=0,periods=N)+pd.to_datetime("2000-01-01").tz_localize('UTC')).values + np.random.randint(10000000000,size=N)
    s = [np.random.randn(len(symbols)) for i in range(len(symbols))]
    #paths = [0.01*np.trunc(np.random.sample(P0))*np.exp(np.cumsum(SIGMA*(0.501-np.random.rand(N))))) for i in range(len(symbols))]
    data = pd.DataFrame(dict(
        seqno=np.arange(0,N,dtype='int64'),
        rowno=np.arange(0,N,dtype='int64')%3,
        exchange=np.random.choice(exchanges, N),
        symbol=np.random.choice(symbols,N),
        receive_time_utc=recv_ts,
        exchange_time_utc=exch_ts          
    ))
    if "_int" in TABLE:
        data = data.assign(
            price_tick=0.01,
            price_i=0.01*np.trunc(100.*np.random.choice(PRICE,N)*np.exp(np.random.randn(N))),
            qty_tick=1,
            qty_i=np.trunc((4.*np.random.rand(N)+1))*VOL # qty
        )
    else: # float
        data = data.assign(
            # np.trunc(100*PRICE*np.exp(np.cumsum(SIGMA*(0.501-np.random.rand(N))))), # price
            price=0.01*np.trunc(100.*np.random.choice(PRICE,N)*np.exp(np.random.randn(N))), # price
            qty=VOL*np.trunc((4.*np.random.rand(N)+1)) # qty
        )
    if "market_by_price" in TABLE:
        data = data.assign(
            action=np.random.choice(["NEW","CHANGE","DELETE"], N), #action
            type=np.random.choice(["BID","ASK"]*50+["TRADE"], N), # type
            side=np.random.choice(["BUY","SELL"], N) # side
        )
    elif "trades" in TABLE:
        data = data.assign(
            side=np.random.choice(["BUY","SELL"], N) # side
        )
    
    stmt='INSERT INTO %s (%s) VALUES'%(TABLE, ",".join(data.columns))
    print("inserting %s rows, stmt: %s"%(N,stmt))
    ts = datetime.now()
    ins_data = [data[x].values for x in data.columns]
    #print("ins_data:", ins_data)
    client.execute(stmt, ins_data, columnar=True)
    ts = datetime.now() - ts
    print("inserted in %s seconds, %f records/s"%(ts.total_seconds(), N/ts.total_seconds()))

stats = client.execute("SELECT table,　sum(bytes) as bytes,　sum(rows) as rows, bytes/rows as bytes_per_row   FROM system.parts　    WHERE active and table=='%s' group by table"%TABLE)[0]
print("table %s nrows %s nbytes %s bytes per row %.2f"%(stats[0], stats[1], stats[2], stats[3]))