# MarketByPrice Clickhouse tables proposal

### Intro
I look at refernce docs here
https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+-+Market+by+Price+-+Multiple+Depth+Book
for MarketByPrice (e.g. Level2) updaes


### MarketByPrice (L2) 

 example  https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+Book+Update+-+New+Best+Price+Entered

In clickhouse, MarketByPrice (L2) information is stored into `market_by_price` table.

[Proposed DDL](https://github.com/mmitkevich/roq-rencap/blob/main/clickhouse/mdc_ddl.sql#L3)

Clickhouse|TAG     |TAG NAME     | VALUE   | DESCRIPTION
--------|--------|-------------|---------|--------------
symbol  |48	| SecurityID  | given   | Unique instrument ID as qualified by the exchangeper tag 22-|SecurityIDSource.
seq     |83	| RptSeq      |e.g. 131	| Sequence number per Instrument update.
type    |269	| MDEntryType |	0       |0 = bid. Type of Market Data entry.
price   |270	| MDEntryPx   | 9427.50 |Price of the Market Data Entry.
quantity|271	| MDEntrySize | 200     |Quantity represented by the Market Data Entry.
action  |279	| MDUpdateAction | 0.   |0 = new. Type of Market Data update action.
num_orders  |346	| NumberOfOrders | 1.   |Number of orders at price level.
n/a     |1023	| MDPriceLevel | 1.     | Position in the book.


#### Problems with persisting MDPriceLevel into database
Consider we had bids

Price |  Size   | PriceLevel
------|----------|----
$100  |  20      |  #1
$99   |  10      |  #2

then,  IncrementalRefresh(X)  comes with MDUpdateAction=New, Price=$101, Size=10

we get now

Price |  Size   | PriceLevel
------|----------|----
$101  | 10       |  #1
$100  |  20      |  #2 (instead of #1)
$99   |  10      |  #3 (instead #2)

Effectively, all level numbering has changed. So we can't persit it to database with append only - each new record mutates this level.
So the easiest way it completely abandon PriceLevel in the clickhouse table. Anyway it could be calculated by reader when rebuilding the book.


### MarketByOrder (L3)

example  https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+-+Market+by+Order+-+Book+Management#MDP3.0MarketbyOrderBookManagement-LimitOrder-NewActionExample

In clickhouse, MarketByOrder (L3) information is stored into MarketByOrder table.

[Proposed DDL is TODO]

 Clickhouse |TAG  | TAG NAME. | VALUE | DESCRIPTION
 -----------|-----|-----------|-------|----
 price      |270. | MDEntryPx | 1010
 order_id   |37	  | OrderID	  | 557	
 display_qty|37706|	MDDisplayQty |	50 	
 order_priority | 37707 |	MDOrderPriority	| 723766	
 action    | 37708 |	OrderUpdateAction/MDUpdateAction |	0 | 	0 = new 1 = change 2 = delete
 type      | 269 | MDEntryType | 0 = bid 1 = ask 2 = trade
 num_orders | 346 | NumberOfOrders | 1 | Number of orders at price level.
          

Notes:
- OrderID, MDOrderPriority are absent in L2 (MarketByPrice)
- OrderUpdateAction is separate tag from MDUPdateAction but has [same values](https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+-+Market+by+Order+-+Book+Management)
- general opaque flags uint64 column could be added as well to store various flags for execution instructions (IOC,etc)

### TradeSummary 
https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+-+Trade+Summary#MDP3.0TradeSummary-TradeSummaryDataSequence
[Proposed DDL](https://github.com/mmitkevich/roq-rencap/blob/main/clickhouse/mdc_ddl.sql#L24)
#### by price repeating group

in clickhouse, trades information is stored into `trades` table. 

[Proposed DDL is TODO]

clickhouse | TAG | TAG NAME | VALUE | DESC
-----------|-----|----------|-------|--------------
.          | 268 | NoMDENtries | . | .
. (const)  | 279 | MDUpdateAction | 0 | 0 = new. Type of Market Data update action.
. (const)  | 269 | MDEntryType | 2 | 2 = trade. Type of Market Data entry. no clickhouse since always 'trade'
symbol     | 48  | SecurityID  | . | Unique instrument ID as qualified by the exchange.
seq        | 83  | RptSeq      | . | Sequence number per Instrument update.
price      | 270 | MDEntryPx   | 9550 | Price of the Market Data Entry.
qty        | 271 | MDEntrySize | . |Aggregated trade quantity for the instrument at the price level represented by the Trade Summary entry.
num_orders | 346 | NumberOfOrders |. |Number of customer orders traded on both sides of the match for the instrument and the price level represented by the entry. 
aggressor_side | 5797 |	AggressorSide | 1 |  0=No aggressor 1=Buy 2=Sell
trade_id   | 37711 | MDTradeEntryID | .| Common Trade ID that links each trade execution.

Notes:
1) this table only differs from MarketByPrice(L2) that it has added `aggressor_side`, `trade_id`. 
2) It is worth to have separate CH table for trades (to be able to quickly doing selects from them separately from MarketByPrice)

### by order repeating groups

TAG | TAG NAME | VALUE | DESC
----|----------|-------|-----
37705 |	NOOrderIDEntries | Repeating group of customer orders that participated in the match.
37 | OrderID	 |  Unique ID assigned by CME Globex to identify orders.
32 | LastQty	 | Qty bought or sold on the fill.

Notes: 
1) aggressive_order_id, passive_order_id could be added to `Trades` table as well (for the case we have L3). In such case we have single `Trades` table with optional identification of two orders matched.


MDUPdateAction, OrderUpdateAction (MarketByOrder)
- New - create/insert a new order (tag 37708-OrderUpdateAction=0 or tag 279-MDUpdateAction=0)
- Update - change order information (tag 37708-OrderUpdateAction=1 or tag 279-MDUpdateAction=1)
- Delete - remove an order (tag 37708-OrderUpdateAction=2 or tag 279-MDUpdateAction=2)


MDUpdateAction (MarketByPrice)
- Add - create/insert a new price at a specified price level (tag 279 MDUpdateAction=0)
- Change - change quantity for a price at a specified price level (tag 279 MDUpdateAction=1)
- Delete - remove a price at a specified price level (tag 279 MDUpdateAction=2)

MDEntryType https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+Book+Update+-+New+Best+Price+Entered

- 0 = Bid
- 1 = Offer
- E = Implied Bid
- F = Implied Offer
- 2 = Trade Summary
