### SecurityDefinition

- FIX SecurityDefinition<d> https://www.onixs.biz/fix-dictionary/4.4/msgtype_d_100.html
- FIX SecurityDefinition<d> Instrument Block https://www.onixs.biz/fix-dictionary/4.4/compBlock_Instrument.html
- CME MDP3 (as provided by EPAM libraries)
- CME MDP3 https://www.cmegroup.com/confluence/display/EPICSANDBOX/MDP+3.0+-+Security+Definition
http://10.1.110.59/epam/MarketDataCollection/blob/master/include/B2BITS_CmeMdpInstrumentDefinition.h#L57

clickhouse.                  | B2B MD P            | FIX                 | roq                         | values
-----------------------------|---------------------|---------------------|-----------------------------|---------------
symbol LC(String)            | symbol_
security_type LowCard(String)| type_               |SecurityType<167>    |                             | FUT, OPT, FXSPOT
security_subtype LC(String)  | subType_            |SecuritySubType<762> |                             | 
cfi_code LC(String)          | cfiCode_            |CFICode<461>         |                             |
seqno UInt64                 | 
receive_time_utc DateTime64(9)|
exchange_time_utc DateTime64(9)|
exchange LC(String)          |
symbol LC(String)            |
description String           |
currency LC(String)          | currency_
settlement_currency String   |
commission_currency String   |
tick_size Float64 DFLT nan   | displayFactor_ or minPriceIncrement?
multiplier Float64 DFLT nan  | 
min_trade_vol Float64 DFLT nan| min_trade_vol_
max_trade_vol Float64 DFLT nan| max_trade_vol_
min_lot_size Float64 DFLT nan | MinLotSize | RoundLot
option_type LC(String)       |
strike_currency LC(String)   | strikeCurrency_
strike_price Float64 DFLT nan | strikePrice_
underlying LC(String)         | 
time_zone LC(String)          |
issue_time_utc DateTime64(9)      | activationTimestamp_
settlement_time_utc DateTime64(9) | 
expiry_time_utc DateTime64(9) | expirationTimestamp_
