## methodology
bench.py script generates random price data
and inserts 500K records into the table (specified as TABLE= in the python code)

## results
### using market_by_price_int

price_tick Gorilla + price_i DoubleDelta

```
CREATE TABLE IF NOT EXISTS market_by_price (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
    type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(LZ4), \
    price_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    price_i Int64 DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    qty_tick Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty_i  Int64  DEFAULT -9223372036854775808 Codec(DoubleDelta, LZ4), \
    num_orders UInt16 Codec(Gorilla,LZ4), \
    trade_id String Codec(LZ4)
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno, rowno);
```

results
```
generating 500000 records...
inserting 500000 rows, stmt: INSERT INTO market_by_price (seqno,rowno,exchange,symbol,price_tick,price_i,qty_tick,qty_i,receive_time_utc,exchange_time_utc,action,type,side) VALUES
inserted in 2.482293 seconds, 201426.664781 records/s
table market_by_price nrows 8894263 nbytes 500000 bytes per row 17.79
```

#### using market_by_price_float
```
CREATE TABLE IF NOT EXISTS market_by_price_float (\
    gateway LowCardinality(String), \
    session_id UUID Codec(LZ4),\
    seqno UInt64 Codec(DoubleDelta, LZ4),\
    rowno UInt32 DEFAULT 0 Codec(DoubleDelta, LZ4),\
    receive_time_utc DateTime64(9) Codec(DoubleDelta, LZ4),\
    exchange_time_utc DateTime64(9) Codec(DoubleDelta, LZ4), \
    snapshot Boolean Codec(LZ4),\
    exchange LowCardinality(String) Codec(LZ4),\
    symbol LowCardinality(String) Codec(LZ4),\
        type Enum8('UNDEFINED' = 0, 'BID' = 48, 'ASK' = 49, 'TRADE' = 50, 'IMPLIED_BID' = 69, 'IMPLIED_ASK' = 70) DEFAULT 'UNDEFINED' Codec(LZ4),\
    action Enum8('UNDEFINED' = 0, 'NEW' = 48, 'CHANGE' = 49, 'DELETE' = 50) DEFAULT 'UNDEFINED' Codec(LZ4),\
    side Enum8('UNDEFINED' = 0, 'BUY' = 48, 'SELL' = 49) DEFAULT 'UNDEFINED' Codec(LZ4), \
    price Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    qty Float64 DEFAULT toFloat64(nan) Codec(Gorilla, LZ4), \
    num_orders UInt16 Codec(Gorilla,LZ4), \
    trade_id String Codec(LZ4)
) ENGINE = MergeTree() \
PARTITION BY toYearWeek(receive_time_utc) \
ORDER BY (receive_time_utc, seqno, rowno) \
PRIMARY KEY (receive_time_utc, seqno, rowno)
```

results

```
roq-rencap/clickhouse/python$ python3 bench.py --number=500000 --repeat 1
generating 500000 records...
inserting 500000 rows, stmt: INSERT INTO market_by_price_float (seqno,rowno,exchange,symbol,receive_time_utc,exchange_time_utc,price,qty,action,type,side) VALUES
inserted in 2.526988 seconds, 197864.018349 records/s
table market_by_price_float nrows 11741417 nbytes 500000 bytes per row 23.48
```