# Comparing B2B, ROQ and Tbricks data structures in the regard to CLICKHOUSE market data storage

### B2Bits CME-MDP3
```c++
/// B2BITS_CmeMdpInstrument.h
    /// Instrument listener.
    /// Instrument listener interface is used to receive instrument events and messages.
    class InstrumentListener
    {
    public:

        /// @name Event notifications
        /// @param instrument Instrument.
        /// @param event Event.
        /// @param NR true if natural recovering is in progress.
        /// @return Control code.
        /// iccContinue to continue processing. 
        /// iccStartRecovery to start recovery of join type.
        /// iccStopRecovery to stop recovery.
        ///@{
        virtual InstrumentControlCode onEvent(Instrument* instrument, const InstrumentEvent& event, bool NR) = 0;

        /** @name Snapshot updates
        * Called on instrument message W
        * Market by order messages
        * @param instrument Instrument.
        * @param message snapshot refresh message.
        * @return Control code.
        * iccContinue to continue processing.
        * @{ */
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBPSnapshotMsg7& message) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOSnapshotMsg7& message) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBPSnapshotMsg9& message) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOSnapshotMsg9& message) { return iccContinue; }

        virtual InstrumentControlCode onSnapshotMessage(Instrument* instrument, const FIXMessage& message) { return iccContinue; }
        /** @} */

        /** @name Realtime updates
        * Called on instrument messages (X, f, R)
        * @param instrument Instrument.
        * @param message realtime message.
        * @param NR true if natural recovering is in progress.
        * @return Control code.
        * iccContinue to continue processing.
        * iccStopRecovery to stop recovery.
        * @{ */
        virtual InstrumentControlCode onMessage(Instrument* instrument, const QuoteRequestMsg& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const SecurityStatusMsg& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncVolumeMsg& message, bool NR) { return iccContinue; }

        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBPIncBookMsg7& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncDailyStatisticsMsg7& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncLimitsBandingMsg7& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncSessionStatisticsMsg7& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncTradeSummaryMsg7& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncTradeSummaryOrderMsg7& message, bool NR) { return iccContinue; }

        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBPIncBookMsg9& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncDailyStatisticsMsg9& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncLimitsBandingMsg9& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncSessionStatisticsMsg9& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncTradeSummaryMsg9& message, bool NR) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const IncTradeSummaryOrderMsg9& message, bool NR) { return iccContinue; }
        
        virtual InstrumentControlCode onIncrementMessage(Instrument* instrument, const FIXMessage& message, bool NR) { return iccContinue; }
        /** @} */

        /** @name MBO book updates
        * Market by order realtime messages
        * @param instrument Instrument.
        * @param message MBO message.
        * @return Control code.
        * iccContinue to continue processing.
        * @{ */
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOIncOrderBookMsg7& message) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOIncBookMsg7& message, const MBPIncBookMsg7& refMsg) { return iccContinue; }

        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOIncOrderBookMsg9& message) { return iccContinue; }
        virtual InstrumentControlCode onMessage(Instrument* instrument, const MBOIncBookMsg9& message, const MBPIncBookMsg9& refMsg) { return iccContinue; }

        virtual InstrumentControlCode onMBOIncOrderBookMessage(Instrument* instrument, const FIXMessage& message) { return iccContinue; }
        virtual InstrumentControlCode onMBOIncBookMessage(Instrument* instrument, const FIXMessage& message, const FIXMessage& refMsg) { return iccContinue; }
        /** @} */   
    protected:

        virtual ~InstrumentListener() {}
    };
```

B2Bits ICE Impact
``` c++

class ApplicationListener
    {
    public:

   /** @addtogroup CB
    * @{ */

        /// onMarketTypesAvail called when market types are available
        /**
        * @param marketTypeSet a set of market types available.
        *
        */
        virtual void onMarketTypesAvail( const IceImpact::AvailMarketTypesSet& marketTypeSet ) {}

        /// onProductDefinitionsAvail called when product definitions requests is completed
        /**
        * @param requestTag request tag that was passed to request function
        * @param requestMarketTypes market types that were requested excluding those which were not actually found at ICE.
        * @param productDirectory product directory that contains product definitions data.
        *
        */
        virtual void onProductDefinitionsAvail( IceImpact::u64 requestTag, const IceImpact::RequestMarketTypesSet& requestMarketTypes, const IceImpact::ProductDirectoryPtr productDirectory ) {}

        /// onBBO user callback is called when top of the book entry is updated for subscribed instrument.
        /// An update is triggered whenever the changes in Bid/Ask/Low/High/Open/Settlement fields
        /// happen (note, changes in Last Trade price/size are just piggy backed with other updates). 
        /// onBBO() callback is called in the context of a thread from internally managed set of threads.
        /// The updates from particular multicast channel are lined up and delivered sequentially
        /// keeping the designated cpu core affined to UDP data flow.
        /**
        *
        * @param ev BBOUpdateEvent
        *
        */
        virtual void onBBO( const IceImpact::BBOUpdateEvent& ev ) {}

        /// onMessage user callback is called when new message is received for subscribed instrument
        /// onMessage callback is called in the context of a thread from internally managed set of threads.
        /// The updates from particular multicast channel are lined up and delivered sequentially
        /// keeping the designated cpu core affined to UDP data flow.
        /**
        * @param event MessageEvent
        * @param isLastMessageInBlock - specifies whether the received message will be the last one and 
        *                               no more messages from the current UDP packet will be delivered
        *                               Use MessageEvent::eventFlowId field to identify previous onMessage events.
        *                               @sa MessageEvent::eventFlowId
        */
        virtual void onMessage( const IceImpact::MessageEvent& event, bool isLastMessageInBlock ) {}

        /// onInstrumentReset user callback is called to inform that the order book of particular instrument 
        /// is to be cleared. If the user does not build their own order books, this event can be skipped.
        /**
        * @param ev - InstrumentResetEvent
        *
        */
        virtual void onInstrumentReset( const IceImpact::InstrumentResetEvent& ev ) {}

        /// onInstrumentRecovered user callback is called to inform that the order book of particular instrument 
        /// is recovered from Snapshot. 
        /**
        * @param ev - InstrumentRecoveryEvent
        *
        */
        virtual void onInstrumentRecovered( const IceImpact::InstrumentRecoveryEvent& ev ) {}

        /// This user callback is called to inform that the message flow on a UDP channel is broken and is restarted.
        /// It usually happens after the seqnuence number loss and recovery.
        /// The user code which depends on particular sequence of ICE messages should reset its state
        /// (e.g. MessageBundleMarker messages, isLastMessageinBlock indicators) 
        /// The eventFlowId member of the event indicates the event flow which is affected.
        /// The value is correlated with eventFlowId of MessageEvent and BBOUpdateEvent.
        /**
        * @param ev - MessageFlowResetEvent
        *
        */
        virtual void onMessageFlowReset( const IceImpact::MessageFlowResetEvent& ev ) {}

        /// onServiceNotification user callback is called when there's service state change. This event is informational.
        /**
        * @param channelId Id of the channel, corresponds to <multicastGroup name="the name"/> in XML file
        * @param bookDepth depth of  book
        * @param type
        *
        */
        virtual void onServiceNotification( const std::string& channelId, IceImpact::BookDepth bookDepth, IceImpact::Notification::Type type ) {}

    /**@}*/

    };
```