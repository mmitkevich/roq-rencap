# some research

## MarketByPrice data collection (clickhouse)
- analysis [MarketByPrice_CLICK](./clickhouse/docs/MarketByPrice_CLICK.md)
- DDL proposal [MarketByPrice mdc_ddl.sql](./clickhouse/ddl/mdc_ddl.sql)
- [ROQ's DDL](./clickhouse/docs/roq_ddl.sql)

### Clickhouse benchmark
- [MarketByPrice insert benchmark](./clickhouse/python/bench.py)

#### using price_i (int64, DoubleDelta), price_tick (Float64, Gorilla)
in a nutshell, about 200K records/second and storage takes about 15 bytes per row (using)
```
generating 500000 records...
inserting 500000 rows, stmt: INSERT INTO market_by_price (seqno,rowno,exchange,symbol,price_tick,price_i,qty_tick,qty_i,receive_time_utc,exchange_time_utc,action,type,side) VALUES
inserted in 2.482293 seconds, 201426.664781 records/s
table market_by_price nrows 8894263 nbytes 500000 bytes per row 17.79
```

## Funding
- python [funding.py](./python/funding/funding.py)

