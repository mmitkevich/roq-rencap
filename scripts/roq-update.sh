#/bin/bash
CHANNEL=https://roq-trading.com/conda/unstable/linux-64
function inst {
  if [[ ! -f $1 || $ROQ_FORCE -gt 0 ]]; then
    echo "download $CHANNEL/$1"
    curl -sko $1 $CHANNEL/$1
    tar xf $1
  else
    echo "skipped $CHANNEL/$1, call with ROQ_FORCE=1"
  fi
} 
inst roq-api-0.7.0-h2bc3f7f_13.tar.bz2
inst roq-client-0.7.0-h2bc3f7f_5.tar.bz2
inst roq-server-0.7.0-h2bc3f7f_17.tar.bz2
inst roq-tools-0.7.0-h2bc3f7f_7.tar.bz2
inst roq-logging-0.7.0-h2bc3f7f_4.tar.bz2
inst roq-deribit-0.7.0-h2bc3f7f_12.tar.bz2
inst roq-bitmex-0.7.0-h2bc3f7f_16.tar.bz2
inst roq-clickhouse-0.7.0-h2bc3f7f_13.tar.bz2
inst roq-fix-bridge-0.6.1-h2bc3f7f_43.tar.bz2
inst roq-oss-http-parser-2.9.2-h6bb024c_0.tar.bz2

