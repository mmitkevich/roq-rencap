#!/bin/bash
ROQ_DIR=$(realpath $(dirname $0)/..)
ROQ_FIX_PORT=${ROQ_FIX_PORT:-3456}
ROQ_FIX_HEARTBEAT_FREQ_SECS=${ROQ_FIX_HEARTBEAT_FREQ_SECS:-1000}

function usage {
    echo \
"Usage:
[ROQ_MODE=real|demo] roq-gateway.sh COMMAND GATEWAY
    COMMAND: start | stop | restart | run | debug
    GATEWAY: bitmex | deribit | fix-bridge"
    exit 1
}

function get_args {
    case $ROQ_APP in
bitmex|deribit)
    ROQ_ARGS="--name $ROQ_APP
    --config_file $ROQ_DIR/$ROQ_MODE/$ROQ_APP.toml
    --client_listen_address $ROQ_DIR/run/$ROQ_APP-$ROQ_MODE.sock
    --rest_uri $ROQ_REST_URI
    --ws_uri $ROQ_WS_URI
    --loop_cpu_affinity=1
    --loop_sleep_nsecs=500
    --loop_timer_freq_nsecs=2500"
;;
fix-bridge)
    ROQ_ARGS="--name $ROQ_APP
    --config_file $ROQ_DIR/$ROQ_MODE/$ROQ_APP.toml
    --client_listen_address $ROQ_FIX_PORT
    --fix_debug=true
    --fix_heartbeat_freq_secs=$ROQ_FIX_HEARTBEAT_FREQ_SECS
    $ROQ_DIR/run/$ROQ_GW-$ROQ_MODE.sock"
;;
*)  usage
    exit 1
    esac
}

function startstop {
    get_args
    ROQ_LOG_FILE="$ROQ_DIR/var/log/$ROQ_APP-$ROQ_MODE.log"
    case $ROQ_ACTION in
run) roq-$ROQ_APP $ROQ_ARGS
    ;;
start) echo "starting $ROQ_APP-$ROQ_MODE"
        ROQ_CMD="$(echo $ROQ_DIR/bin/roq-$ROQ_APP $ROQ_ARGS) > $ROQ_LOG_FILE 2>&1"
        echo "CMD: $ROQ_CMD"
        LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROD_DIR/lib ROQ_v=$ROQ_v start-stop-daemon --start --quiet -d $ROQ_DIR -c $USER --make-pidfile --pidfile $ROQ_DIR/run/$ROQ_APP-$ROQ_MODE.pid  \
            --background --startas /bin/bash -- -c "exec $ROQ_CMD"
        STATUS=$?
        echo "status: $STATUS, pid: $(cat $ROQ_DIR/run/$ROQ_APP-$ROQ_MODE.pid)"
        if [[ $STATUS == 0 ]]; then 
            sleep 8
        fi
        #if [[ $STATUS -ne 0 ]]; then
        #    exit $STATUS
        #fi
    ;;
stop) echo "Stopping $ROQ_APP-$ROQ_MODE" 
        start-stop-daemon --stop --quiet -d $ROQ_DIR -c $USER --oknodo --pidfile $ROQ_DIR/run/$ROQ_APP-$ROQ_MODE.pid
    ;;
clean) echo "clean $ROQ_LOG_FILE"
    mv $ROQ_LOG_FILE $ROQ_LOG_FILE.old
    ;;
*) usage    
    esac
}

if [[ $# -lt 2 || "-$1" == "---help" ]]; then 
    usage
fi

ROQ_ACTION="$1"
shift
ROQ_GW="$1"
shift

ROQ_USE_FIX=${ROQ_FIX:-0}

while [[ $# -gt 0 ]]; do
    case $1 in
-v) ROQ_v=$2
    shift
;;
-m|--mode) ROQ_MODE=$2
    shift
;;
--fix) ROQ_USE_FIX=1
;;
*)  usage
    exit 1
;;
    esac
    shift
done

ROQ_MODE=${ROQ_MODE:-demo}
ROQ_CONFIG_FILE="$ROQ_DIR/$ROQ_MODE/$ROQ_GW.toml"

if [[ ! -f $ROQ_CONFIG_FILE ]]; then
    echo "ERROR: config file '$ROQ_CONFIG_FILE' was not found"
    exit 1
fi

mkdir -p $ROQ_DIR/run
mkdir -p $ROQ_DIR/var/log

set -a
source "$ROQ_DIR/$ROQ_MODE/$ROQ_GW-vars.sh"
set +a

function start_stop_all {
    ROQ_APP=$ROQ_GW startstop
    if [[ "$ROQ_USE_FIX" -ne "0" ]]; then
    ROQ_APP=fix-bridge startstop
    fi
}

if [[ $ROQ_ACTION == "restart" ]]; then
    ROQ_ACTION=stop start_stop_all
    pkill -f roq-$ROQ_GW
    pkill -f roq-fix-bridge
    ROQ_ACTION=start start_stop_all
else
    start_stop_all
fi
