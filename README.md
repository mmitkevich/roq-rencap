### roq-admin

- demo/, real/ -- config files
- bin/ -- scripts
- run/  -- ipc, pidfile
- var/log/ - text logs

### research

[some research here](./research/README.md)